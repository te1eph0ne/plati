/* 
   [[[[[ [[[[[[[ [   [[[[[[  [[[[[[[ [  [[[[[
    [[[[ [     [ [  [      [    [     [ [[[[
    [[[[ [[[[[[  [  [[[[[[[[    [     [ [[[[
   [[[[[ [        [ [      [    [     [ [[[[[
   ..........................................
   simple and efficient static site generator
   ..........................................
*/

#include <iostream>

#define PRG_TITLE    "plati"
#define PRG_VERSION  "0"
#define AUTHOR_NAME  "phone"
#define AUTHOR_EMAIL "denwa@null.net"
                             /* 87654321
                                -------- */
#define ARG_VERSION (1 << 0) // 00000001
#define ARG_HELP    (1 << 1) // 00000010

char getopts(int cnt, const char** vec);
void parseopts(char opts, int argc, const char** argv);
void listcmds();

int main(const int argc, const char** argv) {
  if(argc) {
    char opts = getopts(argc, argv);
    parseopts(opts, argc, argv);
  }
}

char getopts(int cnt, const char** vec) {
  char opts = 0; /* 87654321
	          x_||||||||_ VERSION
	          x__||||||__ HELP
	          x___||||___x
	          x____||____x */
  for(int i = 0; i < cnt; ++i) {
    if(vec[i][0] == '-') {
      if(strcmp(vec[i] + 1, "-version") == 0)
	opts |= ARG_VERSION;
      if(strcmp(vec[i] + 1, "h") == 0 ||
	 strcmp(vec[i] + 1, "-help") == 0)
	opts |= ARG_HELP;
    }
  }
  return opts;
}

void parseopts(char opts, int argc, const char** argv) {
  if(opts & ARG_VERSION) {
    std::cout << PRG_TITLE <<
      " version " << PRG_VERSION << '\n';
    return;
  }
  if(opts & ARG_HELP) {
    std::cout << "usage: " << argv[0] << ' ';
    listcmds();
    return;
  }
}

void listcmds() {
  std::string cmds[] = {"version", "help"};
}
